extends KinematicBody2D

var player_velocity = Vector2(15,0) # Otorga movimiento inicial 

const K_VELOCITY = 15

func _physics_process(delta):
# Checkea si se presionaron teclas 
	get_move()
	if Input.is_action_pressed("touch"):
		player_velocity += (get_global_mouse_position() - position).normalized() * K_VELOCITY
# Aplica las fisicas de movimiento 
	move_and_slide(player_velocity)

func get_move():
	if Input.is_action_pressed("ui_right"): 
		player_velocity.x += K_VELOCITY
	if Input.is_action_pressed("ui_left"): 
		player_velocity.x  -= K_VELOCITY
	if Input.is_action_pressed("ui_up"): 
		player_velocity.y  -= K_VELOCITY
	if Input.is_action_pressed("ui_down"): 
		player_velocity.y += K_VELOCITY

